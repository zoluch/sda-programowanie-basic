import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią: ");
        int usersNumber = scan.nextInt();
        scan.nextLine();
        while (usersNumber < 1) {
            System.out.println("To nie jest liczba dodatnia");
            usersNumber = scan.nextInt();
            scan.nextLine();
        }

        int number1 = 1;
        int number2 = 1;
        int number3 = 0;

        if(usersNumber == 1 || usersNumber == 2) {
            System.out.println(usersNumber + " liczba w ciągu Fibonacciego to " + 1);
        } else {
            for(int i = 3; i <= usersNumber; i++) {
                number3 = number1+number2;
                number1 = number2;
                number2 = number3;
            }
            System.out.println(usersNumber + " liczba w ciągu Fibonacciego to " + number3);
        }
    }
}
