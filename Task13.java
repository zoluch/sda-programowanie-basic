import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Napisz tekst");
        String usersText = scan.nextLine();
        String[] splitText = usersText.split(" ");
        for (int i = 0; i < splitText.length; i++) {
            System.out.print(splitText[i] + " " + splitText[i] + " ");
            if(i == splitText.length-1) {
                System.out.print("\b");
            }
        }
    }
}