import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] usersNumbers = new int[10];
        for(int i = 0; i < usersNumbers.length; i++) {
            System.out.println("Napisz liczbę: ");
            usersNumbers[i] = scan.nextInt();
            scan.nextLine();
        }
        List<Integer> doubledNumbers = new ArrayList<>();
        int numberOfDoubledNumbers = 0;
        for(int i = 0; i < usersNumbers.length-1; i++) {
            for (int j = i + 1; j < usersNumbers.length; j++) {
                if (numberOfDoubledNumbers > 0 && usersNumbers[i] == usersNumbers[j]) {
                    int k = 0;
                    while(usersNumbers[i] != doubledNumbers.get(k)) {
                        k++;
                        if(k == numberOfDoubledNumbers) {
                            doubledNumbers.add(usersNumbers[i]);
                            numberOfDoubledNumbers++;
                        }
                    }
                } else if (usersNumbers[i] == usersNumbers[j]) {
                    doubledNumbers.add(usersNumbers[i]);
                    numberOfDoubledNumbers++;
                }
            }
        }
        System.out.println("Liczby, które wystąpiły co najmniej 2 razy to: ");
        for (Integer doubledNumber : doubledNumbers) {
            System.out.print(" " + doubledNumber);
        }
    }
}