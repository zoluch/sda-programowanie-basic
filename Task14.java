import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj piewrszy małą literę alfabetu łacińskiego");
        char firstSign = scan.next().charAt(0);
        while(!Character.isLowerCase(firstSign)){
            System.out.println("Zły znak, spróbuj jeszcze raz");
            firstSign = scan.next().charAt(0);
        }

        System.out.println("Podaj drugą małą literę alfabetu łacińskiego");
        char secondSign = scan.next().charAt(0);
        while(!Character.isLowerCase(secondSign) || (firstSign == secondSign)) {
            System.out.println("Zły znak, spróbuj jeszcze raz");
            secondSign = scan.next().charAt(0);
        }

        int difference = 0;
        if(firstSign > secondSign) {
            difference = firstSign - secondSign - 1;
        }
        else {
            difference = secondSign - firstSign - 1;
        }
        System.out.println("Pomiędzy tymi znakami są " + difference + " inne znaki");
    }
}