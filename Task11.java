import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        List<String> usersStrings = new ArrayList<>();
        Scanner scan = new Scanner(System.in);
        System.out.println("Wypisuj teksty, napisz \"Starczy\", żeby skończyć");
        usersStrings.add(scan.nextLine());
        int counter = 0;
        if (usersStrings.get(0) == null) {
            System.out.println("Nic nie wpisano");
        } else {
            while (!usersStrings.get(counter).equals("Starczy")) {
                usersStrings.add(scan.nextLine());
                counter++;
            }
        }
        int textLength = 0;
        int newTextLength = 0;
        int indexOfBiggestText = 0;
        for (int i = 1; i < usersStrings.size() - 1; i++) {
            newTextLength = usersStrings.get(i).length();

            if (i == 1) {
                textLength = usersStrings.get(0).length();
                if (textLength - newTextLength >= 0) {
                    indexOfBiggestText = 0;
                } else {
                    indexOfBiggestText = 1;
                }
            }

            if (newTextLength - textLength > 0) {
                textLength = newTextLength;
                indexOfBiggestText = i;
            }
        }
        System.out.println(usersStrings.get(indexOfBiggestText));
    }
}