import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wzrost w cm");
        int heightInCM = scan.nextInt();
        scan.nextLine();
        System.out.println("Podaj wagę w kg");
        float weight = scan.nextFloat();
        float heightinM = (float) heightInCM/100;
        float heightInMSquare = (float) Math.pow(heightinM, 2);
        float BMI = weight/heightInMSquare;
        System.out.println(BMI);
        if(BMI >= 18.5 && BMI <= 24.9) {
            System.out.println("BMI optymalne");
        } else {
            System.out.println("BMI nieoptymalne");
        }
    }
}
