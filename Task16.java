import java.util.Scanner;

public class Task16 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] usersNumbers = new int[10];
        System.out.println("Wypisz liczby: ");
        for(int i = 0; i < usersNumbers.length; i++) {
            usersNumbers[i] = scan.nextInt();
            scan.nextLine();
        }

        int newTotal = 1;
        int total = 0;
        for(int i  = 0; i < usersNumbers.length-1; i++) {
            if(usersNumbers[i] < usersNumbers[i+1]) {
                newTotal++;
            } else {
                newTotal = 1;
            }
            if(newTotal != 1 && newTotal > total) {
                total = newTotal;
            }
        }
        if(total == 0) {
            System.out.println("Ciąg liczb rosnących w tych liczbach wynosi: 0");
        } else {
            System.out.println("Ciąg liczb rosnących w tych liczbach wynosi: " + total);
        }
    }
}