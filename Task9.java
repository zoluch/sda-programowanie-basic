import java.sql.SQLOutput;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią długości fali: ");
        int usersNumber = scan.nextInt();
        scan.nextLine();
        while (usersNumber < 1) {
            System.out.println("To nie jest liczba dodatnia");
            usersNumber = scan.nextInt();
            scan.nextLine();
        }
        int counter = 1;
        int countVertical = 1;
        int spaces = 0;
        int firstLoop = 1;
        StringBuilder firstLane = new StringBuilder(100);
        StringBuilder secondLane = new StringBuilder(100);
        StringBuilder thirdLane = new StringBuilder(100);
        StringBuilder fourthLane = new StringBuilder(100);
        for (int i = 1; i <= usersNumber; i++) {
            if(countVertical%2 == 1) {
                if (counter == 1) {
                    if (spaces != 0) {
                        for (int j = 1; j <= spaces; j++) {
                            firstLane.append(" ");
                        }
                    }
                    firstLane.append("*");
                } else if (counter == 2) {
                    for (int j = 1; j <= spaces; j++) {
                        secondLane.append(" ");
                    }
                    secondLane.append("*");
                } else if (counter == 3) {
                    for (int j = 1; j <= spaces; j++) {
                        thirdLane.append(" ");
                    }
                    thirdLane.append("*");
                } else {
                    if (firstLoop == 1){
                        for (int j = 1; j <= spaces; j++) {
                        fourthLane.append(" ");
                        }
                    } else {
                        for (int j = 1; j <= spaces; j++) {
                            fourthLane.append(" ");
                        }
                        fourthLane.append(" ");
                    }
                    fourthLane.append("*");
                    counter = 0;
                    countVertical++;
                    spaces = 0;
                    firstLoop++;
                }
            } else if(countVertical%2 == 0) {
                if (counter == 1) {
                    fourthLane.append("*");
                } else if (counter == 2) {
                    for (int j = 1; j <= spaces; j++) {
                        thirdLane.append(" ");
                    }
                    thirdLane.append("*");
                } else if (counter == 3) {
                    for (int j = 1; j <= spaces; j++) {
                        secondLane.append(" ");
                    }
                    secondLane.append("*");
                } else {
                    for (int j = 1; j <= spaces; j++) {
                        firstLane.append(" ");
                    }
                    firstLane.append("*");
                    counter = 0;
                    countVertical++;
                    spaces = 0;
                }
            }
            spaces++;
            counter++;
        }
        System.out.println(firstLane);
        System.out.println(secondLane);
        System.out.println(thirdLane);
        System.out.println(fourthLane);
    }
}
