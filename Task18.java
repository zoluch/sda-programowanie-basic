import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wprowadź tekst, który sprawdzi kichnięcie.");
        String usersText = scan.nextLine();

        String ownPattern = "a* psik";
        Pattern pattern = Pattern.compile(ownPattern);
        Matcher matcher = pattern.matcher(usersText);
        boolean matchFound = matcher.find();
        if (matchFound) {
            System.out.println("Kichnąłeś w tekście.");
        } else {
            System.out.println("Nie kichnąłeś w tekście.");
        }
    }
}