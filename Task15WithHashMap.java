import java.util.HashMap;
import java.util.Scanner;

public class Task15WithHashMap {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Integer[] usersNumbers = new Integer[10];
        HashMap<Integer, Integer> myHashMap = new HashMap<>();
        System.out.println("Wypisz 10 liczb: ");
        for(int i = 0; i < usersNumbers.length; i++) {
            int usersNumber = scan.nextInt();
            if(!myHashMap.containsKey(usersNumber)) {
                myHashMap.put(usersNumber, 1);
            } else {
                Integer counter = myHashMap.get(usersNumber);
                myHashMap.put(usersNumber, counter+1);
            }
        }
        System.out.println("Liczby występujace co najmniej 2 razy to: ");
        for(Integer key: myHashMap.keySet()) {
            Integer keyValue = myHashMap.get(key);
            if(keyValue>1) {
                System.out.println(key);
            }
        }
    }
}