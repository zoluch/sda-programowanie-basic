import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią: ");
        int usersNumber = scan.nextInt();
        scan.nextLine();
        while (usersNumber < 1) {
            System.out.println("To nie jest liczba dodatnia");
            usersNumber = scan.nextInt();
            scan.nextLine();
        }
        double total = 0;
        double numberToAdd = 0;
        for (int i = 1; i <= usersNumber; i++) {
            numberToAdd =(double) 1/i;
            total += numberToAdd;
        }

        System.out.println(total);
    }
}
