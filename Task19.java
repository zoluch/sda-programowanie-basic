public class Task19 {
    public static void main(String[] args) {
        Author[] authors = new Author[3];
        authors[0] = new Author("Kowalski", "Polska");
        authors[1] = new Author("Blue", "Irlandia");
        authors[2] = new Author("Schroeder", "Niemcy");
        Poem[] poems = new Poem[3];
        poems[0] = new Poem(authors[0], 4);
        poems[1] = new Poem(authors[1], 5);
        poems[2] = new Poem(authors[2], 4);

        int indexOfPoem = 0;
        int currentBiggestStropheNumbers = poems[0].getStropheNumbers();
        for (int i = 0; i < poems.length - 1; i++) {
            if (currentBiggestStropheNumbers < poems[i + 1].getStropheNumbers()) {
                indexOfPoem = i + 1;
                currentBiggestStropheNumbers = poems[i + 1].getStropheNumbers();
            }
        }
        System.out.println("Wiersz o największej liczbie zwrotek jest autora o nazwisku " + poems[indexOfPoem].getCreator().getSurname());
    }
}