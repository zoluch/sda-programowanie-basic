import java.time.*;
import java.util.Scanner;

public class Task17 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj dzień kolejnego kursu");
        int nextCourseDateDay = scan.nextInt();
        scan.nextLine();
        System.out.println("Podaj miesiąc kolejnego kursu");
        int nextCourseMonth = scan.nextInt();
        scan.nextLine();
        int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int dayOfYearOfNextCourse = 0;
        for (int i = 0; i < nextCourseMonth; i++) {
            if (i == nextCourseMonth - 1) {
                dayOfYearOfNextCourse += nextCourseDateDay;
            } else {
                dayOfYearOfNextCourse += daysInMonth[i];
            }
        }
        LocalDate dateNow = LocalDate.now();
        int dayOfYear = dateNow.getDayOfYear();
        int daysToNextCourse = dayOfYearOfNextCourse - dayOfYear;
        System.out.println("Pozostało " + daysToNextCourse + " dni do następnego kursu");
    }
}