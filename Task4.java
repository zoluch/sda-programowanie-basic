import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią: ");
        int usersNumber = scan.nextInt();
        scan.nextLine();
        while(usersNumber < 1) {
            System.out.println("To nie jest liczba dodatnia");
            usersNumber =  scan.nextInt();
            scan.nextLine();
        }
        for(int i = 1; i <= usersNumber; i++) {
            if(i%3 == 0 && i%7 == 0) {
                System.out.println("pif paf");
            }
             else if(i%3 == 0) {
                System.out.println("pif");
            } else if(i%7 == 0) {
                System.out.println("paf");
            } else {
                System.out.println(i);
            }

        }
    }
}
