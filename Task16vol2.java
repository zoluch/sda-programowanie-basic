import java.util.Scanner;

public class Task16vol2 {
    public static void main(String[] args) {
        System.out.println("Wypisz liczby: ");
        Scanner scan = new Scanner(System.in);
        int totalNumbers = 0;
        int newTotalNumbers = 1;
        int usersNumber = scan.nextInt();
        scan.nextLine();
        int nextUsersNumber;
        for(int i = 0; i<9; i++) {
            nextUsersNumber = scan.nextInt();
            scan.nextLine();
            if(nextUsersNumber > usersNumber) {
                newTotalNumbers++;
            } else {
                totalNumbers = newTotalNumbers;
                newTotalNumbers = 1;
            }
            usersNumber = nextUsersNumber;
        }
        if(newTotalNumbers > totalNumbers) {
            System.out.println("Ciąg liczb rosnących wynosi : " + newTotalNumbers);
        } else if(newTotalNumbers < totalNumbers) {
            System.out.println("Ciąg liczb rosnących wynosi : " + totalNumbers);
        } else {
            System.out.println("Ciąg liczb rosnących wynosi : 0");
        }
    }
}