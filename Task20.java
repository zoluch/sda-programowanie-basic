import java.util.Random;
import java.util.Scanner;

public class Task20 {
    public static void main(String[] args) {
        Random ran = new Random();
        int randomNumber = ran.nextInt(101);
        System.out.println("Wylosowałem liczbę od 0 do 100, zgadnij ją: ");
        Scanner scan = new Scanner(System.in);
        int usersGuess;
        do {
            usersGuess = scan.nextInt();
            scan.nextLine();
            if (usersGuess > randomNumber) {
                System.out.println("Za dużo");
            } else if (usersGuess < randomNumber) {
                System.out.println("Za mało");
            } else {
                System.out.println("Zgadza się! Wylosowana liczba to: " + randomNumber);
            }
        } while (usersGuess != randomNumber);
    }
}