import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Task17WithParse {
    public static void main(String[] args) throws ParseException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj datę kolejnego dnia kursu(dd/mm/yyyy)");
        String nextCourseDateDay = scan.nextLine();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dateToCourse = format.parse(nextCourseDateDay);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateToCourse);
        int dayOfYearCourse = calendar.get(Calendar.DAY_OF_YEAR);
        LocalDate dateNow = LocalDate.now();
        int dayOfYearNow = dateNow.getDayOfYear();
        int daysToNextCourse = dayOfYearCourse - dayOfYearNow;
        System.out.println("Pozostało " + daysToNextCourse + " dni do następnego kursu");
    }
}