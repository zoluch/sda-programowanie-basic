import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą: ");
        float usersNumber1 = scan.nextFloat();
        System.out.println("Podaj znak do operacji na liczbach (+,-,*,/)");
        char usersMathSign = scan.next().charAt(0);
        while(usersMathSign != '+' && usersMathSign != '-' && usersMathSign != '*' && usersMathSign != '/') {
            System.out.println("Błędny znak");
            usersMathSign = scan.next().charAt(0);
        }
        float usersNumber2 = scan.nextFloat();
        while(usersMathSign == '/' && usersNumber2 == 0) {
            System.out.println("Błąd, nie można dzielic przez 0, wprowadź jeszcze raz drugą liczbę");
            usersNumber2 = scan.nextFloat();
        }
        switch (usersMathSign){
            case '+':
                float sum = usersNumber1+usersNumber2;
                System.out.println(usersNumber1+"+"+usersNumber2+"="+sum);
                break;
            case '-':
                float difference = usersNumber1-usersNumber2;
                System.out.println(usersNumber1+"-"+usersNumber2+"="+difference);
                break;
            case '*':
                float product = usersNumber1*usersNumber2;
                System.out.println(usersNumber1+"*"+usersNumber2+"="+product);
                break;
            case '/':
                float quotient = usersNumber1/usersNumber2;
                System.out.println(usersNumber1+"/"+usersNumber2+"="+quotient);
                break;
        }
    }
}
