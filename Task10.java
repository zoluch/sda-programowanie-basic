import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą");
        int usersNumber = scan.nextInt();
        scan.nextLine();
        while (usersNumber < 1) {
            System.out.println("To nie jest liczba dodatnia");
            usersNumber = scan.nextInt();
            scan.nextLine();
        }
        int sum = 0;
        while(usersNumber > 0) {
            sum += usersNumber%10;
            usersNumber = usersNumber/10;
        }

        System.out.println("Suma cyfr liczby to: " + sum);
    }
}
