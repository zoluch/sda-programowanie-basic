import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj tekst");
        String usersString = scan.nextLine();
        int stringLength = usersString.length();
        String usersStringWithoutSpaces = usersString.replaceAll(" ", "");
        int stringWitoutSpacesLength = usersStringWithoutSpaces.length();
        int spaces = stringLength-stringWitoutSpacesLength;
        double percentOfSpacesInText = ((double) spaces/(double) stringLength)*100;

        System.out.println("W Twoim tekście spacje to " + percentOfSpacesInText + "% tekstu");
    }
}