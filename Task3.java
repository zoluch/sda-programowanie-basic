import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj a w równaniu ax^2+bx+c");
        int a = scan.nextInt();
        scan.nextLine();
        System.out.println("Podaj b");
        int b = scan.nextInt();
        scan.nextLine();
        System.out.println("Podaj c");
        int c = scan.nextInt();
        scan.nextLine();
        int delta = (int) ((Math.pow(b, 2)) - (4*a*c));
        if(delta < 0) {
            System.out.println("Delta ujemna");
        } else {
            double x1 = (-b-delta)/(2*a);
            double x2 = (-b+delta)/(2*a);
            System.out.println("x1 = " + x1 + " x2 = " + x2);
        }
    }
}
